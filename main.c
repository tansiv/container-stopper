#define _POSIX_C_SOURCE 200112L // for ftruncate
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <limits.h>

#define ONE_SECOND_NS 1000000000 // 10⁹

static bool set_tansiv_socket_fd(const char *arg, int *fd)
{
	char *endptr;
	errno=0;
	long int strtol_result = strtol(arg, &endptr, 10);
	if (errno!=0 || *endptr!='\0')
		return false;
	if (strtol_result<0 || strtol_result>INT_MAX)
		return false;
	*fd=(int)strtol_result;
	return fd;
}

int main(int argc, char *argv[])
{
	const char freeze_true = '1';
	const char freeze_false = '0';
	const char null_char = '\0';

	int return_value = EXIT_SUCCESS;
	int cgroup_freeze_fd=-1;
	int tansiv_socket_fd=-1;
	int offset_fd=-1;
	if (argc<=3) {
		fprintf(stderr, "Usage: %s <cgroup freeze file path> <TANSIV socket fd (cast int)> <shared offset file path>\n", argv[0]);
		return EXIT_FAILURE;
	}
	if (!set_tansiv_socket_fd(argv[2], &tansiv_socket_fd)) {
		fprintf(stderr, "TANSIV socket fd not understood\n");
		return_value = EXIT_FAILURE;
		goto end;
	}
	cgroup_freeze_fd=open(argv[1], O_WRONLY);
	if (cgroup_freeze_fd==-1) {
		perror("Opening cgroup freeze file failed");
		return_value = EXIT_FAILURE;
		goto end;
	}
	offset_fd = open(argv[3], O_RDWR); // RDWR required by mmap
	if (offset_fd==-1) {
		perror("Opening shared offset file failed");
		return_value = EXIT_FAILURE;
		goto end;
	}
	if (ftruncate(offset_fd, (off_t)sizeof(struct timespec))!=0) {
		perror("Failed to ensure offset file size");
		return_value = EXIT_FAILURE;
		goto end;
	}
	struct timespec *offset = mmap(NULL, sizeof(struct timespec), PROT_WRITE, MAP_SHARED, offset_fd, 0); // will be unmapped on exit
	if (offset==MAP_FAILED) {
		perror("mmap-ing shared offset file failed");
		return_value = EXIT_FAILURE;
		goto end;
	}
	int close_ret = close(offset_fd);
	offset_fd = -1;
	if (close_ret!=0) {
		return_value = EXIT_FAILURE;
		goto end;
	}
	memset(offset, 0, sizeof(struct timespec));

	if (write(tansiv_socket_fd, &null_char, 1)!=1) {
		perror("Signaling TANSIV of readiness failed.");
		return_value = EXIT_FAILURE;
		goto end;
	}

	// main loop
	int readres;
	struct timespec request;
	struct timespec freeze_start;
	bool firstfreeze=true;
	while ((readres=read(tansiv_socket_fd, &request, sizeof(struct timespec)))==sizeof(struct timespec)) {
		// this assumes the cgroup should be thawed as soon as the next deadline is received
		if (!firstfreeze) {
			struct timespec tmp;
			// tmp = current time
			if (clock_gettime(CLOCK_MONOTONIC, &tmp)!=0) {
				perror("clock_gettime"); // this shouldn’t really happen
				return_value=EXIT_FAILURE;
				goto end;
			}
			// tmp = time spent frozen
			tmp.tv_sec  -= freeze_start.tv_sec;
			tmp.tv_nsec -= freeze_start.tv_nsec;
			// tmp = next offset value (non-normalized)
			tmp.tv_sec  += offset->tv_sec;
			tmp.tv_nsec += offset->tv_nsec;
			// normalizing tmp.tv_nsec
			// its range could be ]-1s;2s[
			if (tmp.tv_nsec<0) {
				tmp.tv_nsec += ONE_SECOND_NS;
				tmp.tv_sec  -= 1;
			} else if (tmp.tv_nsec>=ONE_SECOND_NS) {
				tmp.tv_nsec -= ONE_SECOND_NS;
				tmp.tv_sec  += 1;
			}
			*offset = tmp;
		}
		// Thaw the cgroup now that offset is ready
		if (write(cgroup_freeze_fd, &freeze_false, 1)!=1) {
			perror("Thawing the cgroup failed");
			return_value=EXIT_FAILURE;
			goto end;
		}
		// wait until next deadline
		if (nanosleep(&request, NULL)!=0) {
			perror("nanosleep failed"); // don’t know how to handle this
			return_value=EXIT_FAILURE;
			goto end;
		}
		// freeze and get start time
		if (write(cgroup_freeze_fd, &freeze_true, 1)!=1) {
			perror("Freezing the cgroup failed");
			return_value=EXIT_FAILURE;
			goto end;
		}
		if (clock_gettime(CLOCK_MONOTONIC, &freeze_start)!=0) {
			perror("clock_gettime"); // this shouldn’t really happen
			return_value=EXIT_FAILURE;
			goto end;
		}
		// make TANSIV aware that the container is now frozen
		if (write(tansiv_socket_fd, &null_char, 1)!=1) {
			perror("Failed to communicate with TANSIV");
			return_value=EXIT_FAILURE;
			goto end;
		}
		firstfreeze=false;
	}
	if (readres!=0) {
		if (readres==-1)
			perror("read failure");
		return_value=EXIT_FAILURE;
	}

	end:
	if (cgroup_freeze_fd!=-1) {
		if (close(cgroup_freeze_fd)!=0) {
			perror("Closing cgroup freeze file failed");
			if (return_value==EXIT_SUCCESS)
				return_value=EXIT_FAILURE;
		}
	}
	if (offset_fd!=-1) {
		if (close(offset_fd)!=0) {
			perror("Closing shared offset file failed");
			if (return_value==EXIT_SUCCESS)
				return_value=EXIT_FAILURE;
		}
	}
	return return_value;
}
